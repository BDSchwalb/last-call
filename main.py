from flask import Flask, render_template, request, redirect, make_response, url_for, g
import sqlite3
import db
import json
import base64
from datetime import datetime

USERS_TABLE = "Users"
EVENTS_TABLE = "Events"
BUNDLES_TABLE = "Bundles"
app = Flask(__name__)
app.static_folder = 'static'
authenticated = False
session = {"uid" : "","rating" : "",'name': ""}
eventinformation = {"date":"","host":"","Title":"","Description":"","Address":""}
accounts = {}
cart_items = []

eventIDEdited = ""

def latesteventAttendance(cur, eventID):
    attendeesuids = db.returnEventAttendence(cur, eventID)
    invitesuids = db.returnEventInvites(cur, eventID)

    allinviteuids = []
    allattendeesuids = []
    if attendeesuids:
        if len(attendeesuids) == 1:
            pass
        else:
            for uidtuple in invitesuids:
                for uid in uidtuple:
                    allattendeesuids.append(uid)

        attendeesuids = attendeesuids[0]
    if invitesuids:
        for uidtuple in invitesuids:
            for uid in uidtuple:
                allinviteuids.append(uid)

    allattendees = []
    if allattendeesuids == []:
        allattendeesuids = attendeesuids

    for uid in allattendeesuids:
        userinfo = db.returnUser(cur, USERS_TABLE, uid)
        print(userinfo)
        if len(userinfo) > 0:
            username = userinfo[0][2]
            rating = userinfo[0][4]
            user = {'name':username, 'invite_status':1, 'rating':rating}
            allattendees.append(user)

    for uid in allinviteuids:
        print(uid)
        userinfo = db.returnUserByEmail(cur, USERS_TABLE, uid)
        print(userinfo)
        if len(userinfo) > 0:
            username = userinfo[0][2]
            rating = userinfo[0][4]
            user = {'name':username, 'invite_status':0, 'rating':rating}
            if {'name':username, 'invite_status':1, 'rating':rating} not in allattendees:
                allattendees.append(user)
            
    return {'attendees' : allattendees}

def updateDashboard(uid,name,userrating):
    cur = get_db().cursor()
    edata = {'attendees':""}
    latestEvent = db.returnLatestEventForUser(cur, uid)
    allattendess = []
    #Checking if the User has any Events
    if len(latestEvent) > 0:
        latestEvent = db.returnLatestEventForUser(cur, uid)[0]
        allattendees = latesteventAttendance(cur, latestEvent[0])

        print(allattendees)
        eventinformation['Title'] = latestEvent[1]
        eventinformation['Host'] = latestEvent[7]
        eventinformation['Description'] = latestEvent[6]
        eventinformation['Date'] =latestEvent[2]
        eventinformation['Address'] = " ".join([latestEvent[3], latestEvent[4], latestEvent[5] ])
        return render_template('dashboard.html', uid=uid, rating=userrating,name=name, etitle=eventinformation['Title'],ehost="Host: "+eventinformation['Host'],edesc=eventinformation['Description'],edate="Date: "+ eventinformation['Date'],eaddress=eventinformation['Address'], people=allattendees)
    else:
        return render_template('dashboard.html', uid=uid, rating=userrating,name=name, etitle="",ehost="",edesc="",edate="",eaddress="",people=edata)

def convertTime(time):
    print("TIME:{}".format(time) )
    returnStr = ""
    date, splittime = time.split(" ")
    hour,minute = splittime.split(":")
    if int(hour) > 12:
        hour = str(int(hour) - 12)
        returnStr = date + " " + hour + ":" + minute + "PM"
    else:
        returnStr = date + " " + hour + ":" + minute + "AM"
    print("CONVERTED TIME:{}\n\n".format(returnStr) )
    return returnStr

def grabEventList(uid):
    cur = get_db().cursor()
    allevents = db.returnUserAttends(cur,uid)
    returnData = []
    alldatetimes = {}

    for event in allevents:
        alldatetimes[datetime.strptime(event[2], '%m/%d/%Y %H:%M')] = event

    if len(alldatetimes) > 0:
        alldatetimes = sorted(alldatetimes.items(), key=lambda s: s[0])

    for event in alldatetimes:
        eventTuple = event[1]
        eventName = eventTuple[1]
        eventDate = convertTime(eventTuple[2])
        returnData.append({'event_name':eventName, 'date':eventDate})

    return {'events' : returnData}

def grabHostingList(uid):
    cur = get_db().cursor()
    allevents = db.returnUserHostings(cur,uid)
    returnData = []
    alldatetimes = {}
    for event in allevents:
        alldatetimes[ datetime.strptime(event[2], '%m/%d/%Y %H:%M') ] = event
    if len(alldatetimes) > 0:
        alldatetimes = sorted(alldatetimes.items(), key=lambda s: s[0])

    for event in alldatetimes:
        eventTuple = event[1]
        eventName = eventTuple[1]
        eventDate = convertTime(eventTuple[2])
        returnData.append({'event_name':eventName, 'date':eventDate})

    return {'events' : returnData}

def checkTime(time):
    splittime = time.split(" ")
    returnStr = ""
    if len(splittime) == 2:
        if splittime[1].upper() == 'PM':
            #add 12 hours
            hour, minute = splittime[0].split(":")
            hour = str(int(hour) + 12)
            returnStr = hour +  ":" + minute
        elif splittime[1].upper() == 'AM':
            hour, minute = splittime[0].split(":")
            returnStr = hour +  ":" + minute
        
        return returnStr
    
    elif 'AM' in time.upper() or 'PM' in time.upper():
        amORpm = time.upper()[len(time)-2:]
        splittime = time.upper()[:-2]
        if amORpm == 'PM':
            #add 12 hours
            hour, minute = splittime.split(":")
            hour = str(int(hour) + 12)
            returnStr = hour +  ":" + minute
        elif amORpm == 'AM':
            hour, minute = splittime.split(":")
            returnStr = hour +  ":" + minute 
        
        return returnStr   
    else:
        return time


    return returnStr


@app.route('/')
def base():
    return redirect(url_for('home'))

@app.route('/home')
def home():
    return render_template('homepage.html')

@app.route('/about', methods=['GET','POST'])
def about():
    return render_template('about.html')

@app.route('/signup', methods=['GET','POST'])
def SignUp():
    if request.method == 'GET':
        return render_template('signup.html')
    elif request.method == 'POST':
        conn = get_db()
        uid = "".join([request.form['user_email'],":",request.form['user_password']])
        uid = base64.b64encode(uid.encode('utf-8'))
        usertuple = (str(uid)[2:-1], request.form['user_email'] , request.form['user_name'], request.form['user_phone'] , "0.0", request.form['user_password'] )
        result = db.create_user(conn, usertuple)

        if result is not None:
            return redirect(url_for('login') )
        else:
            return redirect(url_for('home') )

@app.route('/login', methods=['GET','POST'])
def login():
    global authenticated
    if request.method == 'POST':
        cur = get_db().cursor()
        result = db.checkforcredentials(cur,request.form['user_email'],request.form['user_password'])
        if len(result) == 1:
            result = result[0]
            authenticated = True
            session['uid'] = result[0]
            session['rating'] = str(result[1])
            session['name'] = result[2]
            print(session)
            return redirect(url_for('dashboard', uid=session['uid'],name=session['name'],userrating=session['rating']) )


        return "no proper credentials"
    elif request.method == 'GET':
        if authenticated:
            return redirect(url_for('dashboard', uid=session['uid'],userrating=session['rating'],name=session['name']))
        else:
            return render_template('login.html')

@app.route('/dashboard/<uid>/<userrating>/<name>')
def dashboard(uid,name,userrating="0.0"):
    global authenticated
    global cart_items
    cart_items = []
    if authenticated:
        return updateDashboard(uid,name,userrating)
    else:
        return redirect(url_for('login'))

@app.route('/logout')
def logout():
    global authenticated
    authenticated = False
    session['uid'] = ""
    session['pass'] = ""
    return redirect(url_for('home'))

@app.route('/events/<uid>')
def events(uid):
    global authenticated
    global cart_items
    cart_items = []
    
    if authenticated:
        eventlist = grabEventList(uid)
        return render_template('events.html',uid=uid, etitle="",ehost="",edesc="",edate="",eaddress="",eventlist=eventlist)
    else:
        return redirect(url_for('login'))

@app.route('/hosting/<uid>')
def hosting(uid):
    global authenticated
    global cart_items
    cart_items = []
    if authenticated:
        eventlist = grabHostingList(uid)
        return render_template('hosting.html',uid=uid, etitle="",ehost="",edesc="",edate="",eaddress="",eventlist=eventlist)
    else:
        return redirect(url_for('login'))

@app.route('/inviteUser/<uid>/<eventName>', methods=['GET','POST'])
def invite(uid, eventName):
    global authenticated
    global cart_items
    cart_items = []
    if authenticated:
        if request.method == "GET":
            return render_template('invite.html',uid=uid, ename=eventName)
        elif request.method == "POST":
            inv_email = request.form['invitee_email'] 
            #SEND INVITE
            conn = get_db()
            cur = conn.cursor()
            result = db.returnUserByEmail(cur, USERS_TABLE, inv_email)
            host = db.returnUser(cur, USERS_TABLE, uid)[0]
            print(eventName)
            if result:
                user = result[0]

                result = db.returnEvent(cur, EVENTS_TABLE, eventName)
                if result:
                    event = result[0]
                    eventID = event[0]
                    print(host[1], user[1], eventID , "\n\n")
                    result = db.create_invites(conn, host[1], user[1], eventID)
            
            return redirect(url_for('hosting', uid=session['uid']) )
            
    else:
        return redirect(url_for('login'))

@app.route('/hosting/<uid>/<eventName>')
def returnHostedEvent(uid, eventName):
    eventlist = grabEventList(uid)
    cur = get_db().cursor()
    print(eventName)
    query = [item for item in db.returnEvent(cur=cur, table=EVENTS_TABLE, Event_Name=eventName)[0]]
    print(query)
    eventinformation['Title'] = query[1]
    eventinformation['Host'] = query[7]
    eventinformation['Description'] = query[6]
    eventinformation['Date'] = convertTime(query[2])
    eventinformation['Address'] = " ".join([query[3], query[4], query[5] ])
    return render_template('hosting.html', uid=uid, etitle=eventinformation['Title'],ehost=eventinformation['Host'],edesc=eventinformation['Description'],edate=eventinformation['Date'],eaddress=eventinformation['Address'],eventlist=eventlist)

@app.route('/delete/<uid>/<eventName>')
def deleteHostedEvent(uid, eventName):
    global authenticated
    if authenticated:
        conn = get_db()
        #DELETE EVENT
        query = [item for item in db.returnEvent(cur=conn.cursor(), table=EVENTS_TABLE, Event_Name=eventName)[0]]
        db.delete_event(conn,query[0])

        eventlist = grabHostingList(uid)
        return redirect(url_for('hosting', uid=uid))#uid=uid, etitle="",ehost="",edesc="",edate="",eaddress="",eventlist=eventlist)
    else:
        return redirect(url_for('login'))

@app.route('/events/<uid>/<eventName>')
def returnEvent(uid, eventName):
    eventlist = grabEventList(uid)
    cur = get_db().cursor()
    query = [item for item in db.returnEvent(cur=cur, table=EVENTS_TABLE, Event_Name=eventName)[0]]
    print(query)
    eventinformation['Title'] = query[1]
    eventinformation['Host'] = query[7]
    eventinformation['Description'] = query[6]
    eventinformation['Date'] = convertTime(query[2])
    eventinformation['Address'] = " ".join([query[3], query[4], query[5] ])
    return render_template('events.html', uid=uid, etitle=eventinformation['Title'],ehost=eventinformation['Host'],edesc=eventinformation['Description'],edate=eventinformation['Date'],eaddress=eventinformation['Address'],eventlist=eventlist)

@app.route('/createAnEvent/<uid>', methods=['GET'])
def createEvent(uid):
    return render_template('createanevent.html', uid=uid)

@app.route('/updateAnEvent/<uid>/<eventName>', methods=['GET','POST'])
def updateAnEvent(uid, eventName):
    global eventIDEdited
    if request.method == 'GET':
        cur = get_db().cursor()
        query = [item for item in db.returnEvent(cur=cur, table=EVENTS_TABLE, Event_Name=eventName)[0]]
        eventIDEdited = query[0]
        eventinformation['Title'] = query[1]
        eventinformation['Host'] = query[7]
        eventinformation['Description'] = query[6]
        timeanddate = query[2].split(" ")
        eventinformation['Time'] = timeanddate[1]
        eventinformation['Date'] = timeanddate[0]
        eventinformation['Address'] = query[3]
        eventinformation['City'],eventinformation['State'] = query[4].split(" ")
        eventinformation['Zip'] = query[5]
        return render_template('update.html', uid=uid, ename=eventinformation['Title'], edate=eventinformation['Date'], etime=eventinformation['Time'],eaddress=eventinformation['Address'],ecity=eventinformation['City'],estate=eventinformation['State'], ezip=eventinformation['Zip'], edesc=eventinformation['Description'])
    elif request.method == 'POST':
        #CREATE EVENT HERE THEN REDIRECT TO HOMEPAGE
        #PARAMETERS IN POST event_name, event_date, event_time, event_address,event_city,event_state,event_zip,event_description,event_privacy
        conn = get_db()
        #EVENT ID IS BASE64 STRING OF "UID:Event_Name"
        eid = eventIDEdited
        datetime = request.form['event_date'] + " " + request.form['event_time'] 
        citystate = request.form['event_city'] + " " + request.form['event_state']
        user = db.returnUser(conn.cursor(), USERS_TABLE, session['uid'])
        host_name = ""
        privacy = 0
        if request.form.get('private'):
            privacy = 1
        if len(user) is not 0:
            host_name = user[0][2]
        
        eventuple = (request.form['event_name'] , datetime, request.form['event_address'] , citystate, request.form['event_zip'],request.form['event_description'],host_name,session['uid'],str(privacy))
        result = db.update_event(conn, eventuple, session['uid'], eid)

        if result is not None:
            eventIDEdited = ""
            return redirect(url_for('hosting', uid=session['uid']) )
        else:
            eventIDEdited = ""
            return redirect(url_for('home') )
        
@app.route('/createAnEvent', methods=['POST'])
def submitEvent():
    if authenticated:
        #CREATE EVENT HERE THEN REDIRECT TO HOMEPAGE
        #PARAMETERS IN POST event_name, event_date, event_time, event_address,event_city,event_state,event_zip,event_description,event_privacy
        conn = get_db()
        #EVENT ID IS BASE64 STRING OF "UID:Event_Name"
        eid = "".join([session['uid'],":",request.form['event_name']])
        eid = base64.b64encode(eid.encode('utf-8'))
        eid = str(eid)[2:-1]    
        time = checkTime( request.form['event_time'] )
        datetime = request.form['event_date'] + " " + time
        citystate = request.form['event_city'] + " " + request.form['event_state']
        user = db.returnUser(conn.cursor(), USERS_TABLE, session['uid'])
        host_name = ""
        privacy = 0
        if request.form.get('private'):
            privacy = 1
        if len(user) is not 0:
            host_name = user[0][2]
        
        eventuple = (eid, request.form['event_name'] , datetime, request.form['event_address'] , citystate, request.form['event_zip'],request.form['event_description'],host_name,session['uid'],str(privacy))
        result = db.create_event(conn, eventuple, session['uid'], eid)

        if result is not None:
            return updateDashboard(session['uid'],session['name'],session['rating'])
        else:
            return redirect(url_for('home') )
    else:
        return redirect(url_for('login') )

@app.route('/store/<uid>')
def store(uid):
    global cart_items
    cart_items = []
    if authenticated:
        cur = get_db().cursor()
        bundles = db.returnAll(cur,BUNDLES_TABLE)
        data = []
        for bundle in bundles:
            contents = bundle[2].split(";")
            data.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})

        bundleData = {'bundles':data, 'cart':cart_items}
        print(bundleData)
        return render_template('store.html', uid=uid, bundleData=bundleData)
    else:
        cart_items = []
        return redirect(url_for('login') )

@app.route('/store/<uid>/add/<bundle_name>')
def addtoCart(uid,bundle_name):
    global cart_items
    if authenticated:
        cur = get_db().cursor()
        bundles = db.returnAll(cur,BUNDLES_TABLE)
        data = []
        for bundle in bundles:
            contents = bundle[2].split(";")
            data.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})
            if bundle[3] == bundle_name:
                cart_items.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})
        
        bundleData = {'bundles':data, 'cart':cart_items}
        return render_template('store.html', uid=uid, bundleData=bundleData)
    else:
        cart_items = []
        return redirect(url_for('login') )

@app.route('/store/<uid>/<cat>')
def storeCategory(uid, cat):
    if authenticated:
        cur = get_db().cursor()
        bundles = db.returnAll(cur,BUNDLES_TABLE)
        data = []
        if cat == 'A_Bundles':
            for bundle in bundles:
                if(bundle[3] == 'Frat Attack'):
                    contents = bundle[2].split(";")
                    data.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})
                    tempTitle1 = bundle[3]
                    tempPrice1 = bundle[1]
                if(bundle[3] == 'Girly Pops'):
                    contents = bundle[2].split(";")
                    data.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})
                    tempTitle2 = bundle[3]
                    tempPrice2 = bundle[1]
        else:
            for bundle in bundles:
                if(bundle[3] == 'Take it down a notch'):
                    contents = bundle[2].split(";")
                    data.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})
                    tempTitle1 = bundle[3]
                    tempPrice1 = bundle[1]
                if(bundle[3] == 'Filthy Animals'):
                    contents = bundle[2].split(";")
                    data.append({'bundle_name': bundle[3],'bundle_price': bundle[1],'bundle_contents': contents})
                    tempTitle2 = bundle[3]
                    tempPrice2 = bundle[1]
        bundleData = {'bundles':data}
        return render_template('store.html', uid=uid, bundleData=bundleData , ItemTitle1 =tempTitle1, price1=tempPrice1, ItemTitle2 =tempTitle2, price2=tempPrice2)
    else:
        return redirect(url_for('login') )

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect("sqlite/lastcall.db")
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
