import sqlite3
from sqlite3 import Error
from datetime import datetime

USERS_TABLE = "Users"
EVENTS_TABLE = "Events"
BUNDLES_TABLE = "Bundles"
ATTENDS_TABLE = "Attends"
INVITES_TABLE = "Invites"
BUYS_TABLE = "Buys"
CREATES_TABLE = "Creates"

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None

connector = create_connection("sqlite/lastcall.db")

def importSampleUserData():
    allusers = []
    for line in open("sampleuserdata.txt"):
        allusers.append((line.strip().split(",")))
    return allusers

def importSampleEventData():
    allevents = []
    for line in open("sampleeventdata.txt"):
        allevents.append((line.strip().split(",")))
    return allevents

def importSampleBundleData():
    allbundles = []
    for line in open("samplebundledata.txt"):
        allbundles.append((line.strip().split(",")))
    return allbundles

def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def create_user(conn, usertuple):
    sqlstatement = ''' INSERT INTO Users(UID, Email, Name, Phone_Number, Rating, Password)
              VALUES(?,?,?,?,?,?) '''
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement, usertuple)
        conn.commit()
        return cur.lastrowid

    except Error as e:
        print(e)
        return None

def delete_user(conn, uid):
    # If the user is deleted, so are all the events the user is hosting
    # The other relations tables with the event and user should delete automatically because of CASCADE
    sqlstatement1 = "DELETE FROM Events WHERE Host_UID = " + uid + ";"
    sqlstatement2 = "DELETE FROM Users WHERE UID = " + uid + ";"
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement1, usertuple)
        cur.execute(sqlstatement2, usertuple)
        return cur.lastrowid

    except Error as e:
        print(e)
        return None

def create_event(conn, eventtuple, uid, eventID):
    # Only the person hosting can "add" and event to the events table. They are also attending event by default
    sqlstatement = '''INSERT INTO Events(Event_ID, Event_Name, Time, Address, State, Zip, Description, Host, Host_UID, Privacy)
              VALUES(?,?,?,?,?,?,?,?,?,?);'''
    sqlstatement1 = '''INSERT INTO Creates VALUES (?, ?);'''
    sqlstatement2 = '''INSERT INTO Attends VALUES (?, ?);'''
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement, eventtuple)
        cur.execute(sqlstatement1, (uid, eventID) )
        cur.execute(sqlstatement2, (uid, eventID) )
        conn.commit()
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def update_event(conn, eventtuple, uid, eventID):
    # Only the person hosting can "add" and event to the events table. They are also attending event by default
    sqlstatement = "UPDATE Events SET Event_Name=?, Time=?, Address=?, State=?, Zip=?, Description=?, Host=?, Host_UID=?, Privacy=? WHERE Event_ID='" + eventID + "' "
    # '''INSERT INTO Events(Event_ID, Event_Name, Time, Address, State, Zip, Description, Host, Host_UID, Privacy)
    #           VALUES(?,?,?,?,?,?,?,?,?,?);'''

    cur = conn.cursor()
    try:
        cur.execute(sqlstatement, eventtuple)
        conn.commit()
        return cur.lastrowid
    except Error as e:
        print(e)
        return None


def delete_event(conn, eventID):
    sqlstatement = "DELETE From Events WHERE Event_ID = '" + eventID + "';"
    # The other relations tables with the event should delete automatically because of CASCADE
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement)
        conn.commit()
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def create_bundle(conn, bundletuple):
    sqlstatement = ''' INSERT INTO Bundles(Bundle_ID, Price, Contents, Name, Age_Restrictions)
              VALUES(?,?,?,?,?) '''
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement, bundletuple)
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def delete_bundle(conn, bundleID):
    sqlstatement = "DELETE FROM Bundles WHERE Bundle_ID = " + bundleID + ";"
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement)
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def create_invites(conn, inviter_email, invitee_email, eventID):
    sqlstatement = '''INSERT INTO Invites(Email_Inviter,Email_Invitee,Event_ID_Invited) Values (?,?,?);'''
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement, (inviter_email, invitee_email, eventID) )
        conn.commit()
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def delete_invites(conn, inviter_email, invitee_email, eventID):
    sqlstatement = "DELETE FROM Invites WHERE Email_Inviter = " + inviter_email + " AND Email_Invitee = " + invitee_email + " AND Event_ID_Invited = " + eventID + ";"
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement)
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def create_attends(conn, uid, eventID):
    # When a user accepts an invite or creates an event
    sqlstatement = '''INSERT INTO Attends VALUES (?, ?);'''
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement, uid, eventID)
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

def delete_attends(conn, uid, eventID):
    sqlstatement = "DELETE FROM Attends WHERE Email_Attends = '" + uid + "' AND Event_ID_Attends = '" + eventID + "';"
    cur = conn.cursor()
    try:
        cur.execute(sqlstatement)
        return cur.lastrowid
    except Error as e:
        print(e)
        return None

""" Database 'Views' """

def returnUserHostings(cur, uid):
    # Shows all events that the user is hosting
    sql = "SELECT * FROM Events WHERE Host_UID = '" + uid + "';"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnUserAttends(cur, uid):
    # Displays all events the user is attending including the ones created/hosting
    sql = "SELECT * FROM Events, Attends WHERE Event_ID_Attends = Event_ID AND Email_Attends = '" + uid + "';"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnEventInvites(cur, eventID):
    # To display all people invited to a particular event
    sql = "SELECT Email_Invitee FROM Invites WHERE Event_ID_Invited = '" + eventID + "';"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnEventAttendence(cur, eventID):
    # To display all people attending a particular event
    sql = "SELECT Email_Attends FROM Attends WHERE Event_ID_Attends = '" + eventID + "';"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnAll(cur, table):
    sql = "SELECT * FROM " + table
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnUser(cur, table, uid):
    sql = "SELECT * FROM " + table + " WHERE UID = '" + uid + "'"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnUserByEmail(cur, table, email):
    sql = "SELECT * FROM " + table + " WHERE Email = '" + email + "'"
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def returnEvent(cur, table, Event_Name):

    sql = "SELECT * FROM " + table + " WHERE Event_Name = \"" + Event_Name + "\""
    print(sql)
    try:
        cur.execute(sql)
        rows = cur.fetchall()
        return rows
    except:
        print("COULD NOT FIND ANYTHING")
        return None

def checkforcredentials(cur, email, password):
    sql = "SELECT UID, Rating, Name FROM Users WHERE Email = '" + email + "' AND Password = '" + password + "' "
    cur.execute(sql)
    rows = cur.fetchall()
    print(rows)
    return rows

def returnLatestEventForUser(cur, uid):
    # Shows latest Event for user
    alleventsofUser = returnUserAttends(cur, uid)
    rows = []
    alldatetimes = {}
    for event in alleventsofUser:
        alldatetimes[datetime.strptime(event[2], '%m/%d/%Y %H:%M')] = event
    if len(alldatetimes) > 0:
        rows = [alldatetimes[min(alldatetimes)]]

    return rows

def setupDatabase():
    global connector
    dbconnector = connector
    allusers = importSampleUserData()
    allevents = importSampleEventData()
    allbundles = importSampleBundleData()

    # Users(Email, Name, Phone_Number, Rating, Password, Event_List)
    create_Users_table = """ CREATE TABLE IF NOT EXISTS Users (
                                        UID text PRIMARY KEY,
                                        Email text,
                                        Name text NOT NULL,
                                        Phone_Number TEXT NOT NULL,
                                        Rating FLOAT DEFAULT 0.0,
                                        Password TEXT NOT NULL
                                    );
                                  """
    #Events(Event_ID, Event_Name, Time, Address, State, Zip, Description, Host, Privacy)
    create_Events_table = """CREATE TABLE IF NOT EXISTS Events (
                                    Event_ID TEXT NOT NULL UNIQUE PRIMARY KEY,
                                    Event_Name TEXT,
                                    Time TEXT DEFAULT NULL,
                                    Address TEXT DEFAULT NULL,
                                    State TEXT DEFAULT NULL,
                                    Zip TEXT DEFAULT NULL,
                                    Description TEXT,
                                    Host TEXT NOT NULL,
                                    Host_UID TEXT NOT NULL,
                                    Privacy TEXT NOT NULL
                                    );
                                    """
    #Bundles(Bundle_ID, Price, Contents, Name, Age_Restrictions)
    create_Bundles_table = """CREATE TABLE IF NOT EXISTS Bundles (
                                        Bundle_ID TEXT UNIQUE PRIMARY KEY,
                                        Price TEXT,
                                        Contents TEXT,
                                        Name TEXT,
                                        Age_Restrictions TEXT
                                    );
                                    """
    #Invites(Email, Email, Event_ID)
    create_Invites_table = """ CREATE TABLE IF NOT EXISTS Invites (
                                        Email_Inviter TEXT NOT NULL,
                                        Email_Invitee TEXT NOT NULL,
                                        Event_ID_Invited TEXT NOT NULL,
                                        FOREIGN KEY(Email_Inviter) REFERENCES Users(Email)
                                                ON DELETE CASCADE,
                                        FOREIGN KEY(Email_Invitee) REFERENCES Users(Email)
                                                ON DELETE CASCADE,
                                        FOREIGN KEY(Event_ID_Invited) REFERENCES Events(Event_ID)
                                                ON DELETE CASCADE
                                    );
                                    """
     #Creates(Email, Event_ID)
    #Creates(Email, Event_ID)
    create_Creates_table = """ CREATE TABLE IF NOT EXISTS Creates (
                                        Email_Creates TEXT NOT NULL,
                                        Event_ID_Creates TEXT NOT NULL,
                                        FOREIGN KEY(Email_Creates) REFERENCES Users(UID)
                                                ON DELETE CASCADE,
                                        FOREIGN KEY(Event_ID_Creates) REFERENCES Events(Event_ID)
                                                ON DELETE CASCADE
                                    );
                                    """
    #Attends(Email, Event_ID)
    create_Attends_table = """ CREATE TABLE IF NOT EXISTS Attends (
                                        Email_Attends TEXT NOT NULL,
                                        Event_ID_Attends TEXT NOT NULL,
                                        FOREIGN KEY(Email_Attends) REFERENCES Users(UID)
                                                ON DELETE CASCADE,
                                        FOREIGN KEY(Event_ID_Attends) REFERENCES Events(Event_ID)
                                                ON DELETE CASCADE
                                    );
                                    """
    #Buys(Email, Bundle_ID)
    create_Buys_table = """ CREATE TABLE IF NOT EXISTS Buys (
                                        Email_Buys TEXT UNIQUE NOT NULL,
                                        Bundle_ID_Buys TEXT UNIQUE NOT NULL,
                                        FOREIGN KEY(Email_Buys) REFERENCES Users(Email)
                                                ON DELETE CASCADE,
                                        FOREIGN KEY(Bundle_ID_Buys) REFERENCES Bundles(Bundle_ID)
                                                ON DELETE CASCADE
                                    );
                                    """


    create_table(dbconnector, create_Users_table)
    print("created users table")
    create_table(dbconnector, create_Events_table)
    print("created events table")
    create_table(dbconnector, create_Bundles_table)
    print("created bundles table")
    create_table(dbconnector, create_Invites_table)
    print("created invites table")
    create_table(dbconnector, create_Creates_table)
    print("created creates table")
    create_table(dbconnector, create_Attends_table)
    print("created attends table")
    create_table(dbconnector, create_Attends_table)
    print("created attends table")
    create_table(dbconnector, create_Buys_table)
    print("created buys table")

    """
    Creating New Users, events and Bundles
    """

    for user,event in zip(allusers,allevents):
        create_user(dbconnector, user)
        create_event(dbconnector, event, event[8], event[0])

    for bundle in allbundles:
        create_bundle(dbconnector, bundle)

def testdatabase():
    global connector
    dbconnector = connector

    print("\n\nSELECTING ALL USERS AND PRINTING THEIR NAMES")
    allInformation = returnAll(dbconnector.cursor(), USERS_TABLE)
    for info in allInformation:
        allevents = returnUserHostings(dbconnector.cursor(), info[0])
        print(info[2] + " is hosting: " + "".join(["\n\t" + event[0] + " at " + event[1] for event in allevents]) )

    print("\n\nSELECTING ALL USERS AND PRINTING THEIR NAMES")
    allInformation = returnAll(dbconnector.cursor(), USERS_TABLE)
    for info in allInformation:
        allevents = returnUserAttends(dbconnector.cursor(), info[0])
        print(info[2] + " is attending: " + "".join(["\n\t" + event[1] for event in allevents]) )


    print("\n\nSELECTING ALL EVENTS AND PRINTING THEIR NAMES")
    allInformation = returnAll(dbconnector.cursor(), EVENTS_TABLE)
    for info in allInformation:
        print(info[1])

    print("\n\nSELECTING ALL BUNDLES AND PRINTING THEIR NAMES")
    allInformation = returnAll(dbconnector.cursor(), BUNDLES_TABLE)
    for info in allInformation:
        print(info[3])
    print("\n\n")

setupDatabase()
testdatabase()
connector.commit()
connector.cursor().close()
